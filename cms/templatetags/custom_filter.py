from django import template
import json

register = template.Library()

@register.filter
def trimStr(str,lim):
    return str[:lim]