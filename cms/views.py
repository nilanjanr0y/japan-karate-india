import os
import epoch
from django.conf import settings
from visitors.models import *

from google.cloud import storage

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.renderers import TemplateHTMLRenderer
from django.http.response import HttpResponseRedirect
from rest_framework.reverse import reverse_lazy

# Google cloud storage api bucket declaration
gclient = storage.Client()
bucket = gclient.get_bucket(settings.GC_BUCKET)

# Create your views here.

'''
    @name: banner
    @param : self, request, format 
    @description : banner editor for admin
'''   
class BannerSection(APIView):
    renderer_classes = [TemplateHTMLRenderer]
    template_name = 'cms/banner.html'
#     @method_decorator(login_required)

    def get(self, request):
        context = {'title': 'Edit Banner',
                   'gcburl': settings.GC_MEDIA_STORAGE,
                   'data': HomeBlocks.objects.get(name='banner')}
        return Response(context)
    
    def post(self, request):
        title = request.POST.get('banner_text',None)
        media_path =  settings.MEDIA_URL
        img_dir = os.path.join(media_path,"images")
        sect_data = HomeBlocks.objects.get(name='banner')
        sect_data.title = title
        if bool(request.FILES.get('banner_bg', False)) == True:
            sect_bg_in_mem = request.FILES['banner_bg']
            bg_blob = bucket.blob(img_dir+'/'+str(epoch.now())+'.jpg')
            bg_blob.upload_from_string(sect_bg_in_mem.read(),content_type=sect_bg_in_mem.content_type)
            sect_data.featured_img=bg_blob.public_url.replace(settings.GC_MEDIA_STORAGE,'')
        sect_data.save()    
                        
        return HttpResponseRedirect(reverse_lazy('banner'))

'''
    @name: philosophy
    @param : self, request, format 
    @description : philosophy editor for admin
'''   
class PhiloSection(APIView):
    renderer_classes = [TemplateHTMLRenderer]
    template_name = 'cms/philosophy.html'
#     @method_decorator(login_required)

    def get(self, request):
        context = {'title': 'Edit Philosophy',
                   'gcburl': settings.GC_MEDIA_STORAGE,
                   'data': HomeBlocks.objects.get(name='philosophy'),
                   'arr': Philosophy.objects.all()}
        return Response(context)
    
    def post(self, request):
        titles = request.POST.getlist('clause_title',None)
        body = request.POST.getlist('clause_body',None)
        for i,title in enumerate(titles) :
            clause_data = Philosophy.objects.get(pk=i+1)
            clause_data.title=title
            clause_data.description=body[i]
            clause_data.save()
        media_path =  settings.MEDIA_URL
        img_dir = os.path.join(media_path,"images")
        sect_data = HomeBlocks.objects.get(name='philosophy')
        if bool(request.FILES.get('section_bg', False)) == True:
            sect_bg_in_mem = request.FILES['section_bg']
            bg_blob = bucket.blob(img_dir+'/'+str(epoch.now())+'.jpg')
            bg_blob.upload_from_string(sect_bg_in_mem.read(),content_type=sect_bg_in_mem.content_type)
            sect_data.featured_img=bg_blob.public_url.replace(settings.GC_MEDIA_STORAGE,'')
        sect_data.save()    
                        
        return HttpResponseRedirect(reverse_lazy('philosophy'))


'''
    @name: accolades
    @param : self, request, format 
    @description : accolades editor for admin
'''   
class AccoSection(APIView):
    renderer_classes = [TemplateHTMLRenderer]
    template_name = 'cms/accolades.html'
#     @method_decorator(login_required)

    def get(self, request):
        context = {'title': 'Edit Accolades',
                   'gcburl': settings.GC_MEDIA_STORAGE,
                   'data': HomeBlocks.objects.get(name='accolades'),
                   'arr': Accolades.objects.all()}
        return Response(context)
    
    def post(self, request):
        if request.POST.get('type',None) == 'section':
            sect_title = request.POST.get('sect_title',None)
            sect_subtitle = request.POST.get('sect_subtitle',None)
            media_path =  settings.MEDIA_URL
            img_dir = os.path.join(media_path,"images")
            sect_data = HomeBlocks.objects.get(name='accolades')
            sect_data.title = sect_title
            sect_data.subtitle = sect_subtitle
            if bool(request.FILES.get('section_bg', False)) == True:
                sect_bg_in_mem = request.FILES['section_bg']
                bg_blob = bucket.blob(img_dir+'/'+str(epoch.now())+'.jpg')
                bg_blob.upload_from_string(sect_bg_in_mem.read(),content_type=sect_bg_in_mem.content_type)
                sect_data.featured_img=bg_blob.public_url.replace(settings.GC_MEDIA_STORAGE,'')
            sect_data.save()    
        else:
            acc_title = request.POST.get('acc_title',None)
            acc_desc = request.POST.get('acc_desc',None)
            acc_ft = False if request.POST.get('acc_ft') is None else True
            media_path =  settings.MEDIA_URL
            acc_dir = os.path.join(media_path,"accolades")
            acc_data = Accolades(
                                block = HomeBlocks.objects.get(name='accolades'),
                                title = acc_title,
                                description = acc_desc,
                                feature = acc_ft
                                )
            if bool(request.FILES.get('acc_img', False)) == True:
                acc_img_in_mem = request.FILES['acc_img']
                img_blob = bucket.blob(acc_dir+'/'+str(epoch.now())+'.jpg')
                img_blob.upload_from_string(acc_img_in_mem.read(),content_type=acc_img_in_mem.content_type)
                acc_data.image=img_blob.public_url.replace(settings.GC_MEDIA_STORAGE,'')
            acc_data.save()    
                        
        return HttpResponseRedirect(reverse_lazy('accolades'))



'''
    @name: notices
    @param : self, request, format 
    @description : notices editor for admin
'''   
class AnnSection(APIView):
    renderer_classes = [TemplateHTMLRenderer]
    template_name = 'cms/notices.html'
#     @method_decorator(login_required)

    def get(self, request):
        context = {'title': 'Edit Notices',
                   'gcburl': settings.GC_MEDIA_STORAGE,
                   'data': HomeBlocks.objects.get(name='notices'),
                   'arr': Notices.objects.all()}
        return Response(context)
    
    def post(self, request):
        if request.POST.get('type',None) == 'section':
            sect_title = request.POST.get('sect_title',None)
            sect_subtitle = request.POST.get('sect_subtitle',None)
            media_path =  settings.MEDIA_URL
            img_dir = os.path.join(media_path,"images")
            sect_data = HomeBlocks.objects.get(name='notices')
            sect_data.title = sect_title
            sect_data.subtitle = sect_subtitle
            sect_data.save()    
        else:
            ann_title = request.POST.get('ann_title',None)
            ann_desc = request.POST.get('ann_desc',None)
            ann_ft = False if request.POST.get('ann_ft') is None else True
            media_path =  settings.MEDIA_URL
            ann_dir = os.path.join(media_path,"notices")
            ann_data = Notices(
                                block = HomeBlocks.objects.get(name='notices'),
                                title = ann_title,
                                description = ann_desc,
                                feature = ann_ft
                                )
            if bool(request.FILES.get('ann_img', False)) == True:
                ann_img_in_mem = request.FILES['ann_img']
                img_blob = bucket.blob(ann_dir+'/'+str(epoch.now())+'.jpg')
                img_blob.upload_from_string(ann_img_in_mem.read(),content_type=ann_img_in_mem.content_type)
                ann_data.image=img_blob.public_url.replace(settings.GC_MEDIA_STORAGE,'')
            ann_data.save()    
                        
        return HttpResponseRedirect(reverse_lazy('notices'))
    


'''
    @name: notices
    @param : self, request, format 
    @description : notices editor for admin
'''   
class TrainerSection(APIView):
    renderer_classes = [TemplateHTMLRenderer]
    template_name = 'cms/trainers.html'
#     @method_decorator(login_required)

    def get(self, request):
        context = {'title': 'Edit Trainers',
                   'gcburl': settings.GC_MEDIA_STORAGE,
                   'data': HomeBlocks.objects.get(name='trainers'),
                   'arr': Trainers.objects.all()}
        return Response(context)
    
    def post(self, request):
        if request.POST.get('type',None) == 'section':
            sect_title = request.POST.get('sect_title',None)
            sect_subtitle = request.POST.get('sect_subtitle',None)
            media_path =  settings.MEDIA_URL
            img_dir = os.path.join(media_path,"images")
            sect_data = HomeBlocks.objects.get(name='trainers')
            sect_data.title = sect_title
            sect_data.subtitle = sect_subtitle
            sect_data.save()    
        else:
            trn_name = request.POST.get('trn_name',None)
            trn_bio = request.POST.get('trn_bio',None)
            trn_desig = request.POST.get('trn_desig',None)
            trn_email = request.POST.get('trn_email',None)
            trn_phone = request.POST.get('trn_phone',None)
            trn_fb = request.POST.get('trn_fb',None)
            trn_ft = False if request.POST.get('trn_ft') is None else True
            media_path =  settings.MEDIA_URL
            trn_dir = os.path.join(media_path,"trainers")
            trn_data = Trainers(
                                block = HomeBlocks.objects.get(name='trainers'),
                                name = trn_name,
                                bio = trn_bio,
                                designation = trn_desig,
                                email = trn_email,
                                phone = trn_phone,
                                facebook = trn_fb,
                                feature = trn_ft
                                )
            if bool(request.FILES.get('trn_img', False)) == True:
                trn_img_in_mem = request.FILES['trn_img']
                img_blob = bucket.blob(trn_dir+'/'+str(epoch.now())+'.jpg')
                img_blob.upload_from_string(trn_img_in_mem.read(),content_type=trn_img_in_mem.content_type)
                trn_data.image=img_blob.public_url.replace(settings.GC_MEDIA_STORAGE,'')
            trn_data.save()    
                        
        return HttpResponseRedirect(reverse_lazy('trainers'))
    
    
'''
    @name: footer
    @param : self, request, format 
    @description : footer info editor for admin
'''   
class FooterSection(APIView):
    renderer_classes = [TemplateHTMLRenderer]
    template_name = 'cms/footer.html'
#     @method_decorator(login_required)

    def get(self, request):
        context = {'title': 'Edit Footer info',
                   'gcburl': settings.GC_MEDIA_STORAGE,
                   'data': Footer.objects.get(pk=1)}
        return Response(context)
    
    def post(self, request):
        if request.POST.get('type',None) == 'section':
            about = request.POST.get('about',None)
            address = request.POST.get('address',None)
            gmap = request.POST.get('gmap',None)
            phone1 = request.POST.get('phone1',None)
            phone2 = request.POST.get('phone2',None)
            email1 = request.POST.get('email1',None)
            email2 = request.POST.get('email2',None)
            facebook = request.POST.get('facebook',None)
            sect_data = Footer.objects.get(pk=1)
            sect_data.about = about
            sect_data.address = address
            sect_data.gmap = gmap
            sect_data.phone1 = phone1
            sect_data.phone2 = phone2
            sect_data.email1 = email1
            sect_data.email2 = email2
            sect_data.facebook = facebook
            sect_data.save()      
                        
        return HttpResponseRedirect(reverse_lazy('footer'))
    
    
'''
    @name: office details
    @param : self, request, format 
    @description : office details editor for admin
'''   
class OfficesSection(APIView):
    renderer_classes = [TemplateHTMLRenderer]
    template_name = 'cms/offices.html'
#     @method_decorator(login_required)

    def get(self, request):
        context = {'title': 'Edit Office data',
                   'edit': request.data.get('edit',None),
                   'gcburl': settings.GC_MEDIA_STORAGE,
                   'data': Offices.objects.all()}
        if request.data.get('edit',None) is not None :
            context.target = Offices.objects.get(pk=request.data.get('edit',None))
        return Response(context)
    
    def post(self, request):
        if request.data.get('edit',None) is None:
            Office = Offices( title = request.POST.get('off_title',None) )
        else:
            Office = Offices.objects.get(pk=request.data.get('edit',None))
            Office.title = request.POST.get('off_title',None)
        Office.address = request.POST.get('address',None)
        Office.gmap = request.POST.get('gmap',None)
        Office.person = request.POST.get('person',None)
        Office.phone1 = request.POST.get('phone1',None)
        Office.phone2 = request.POST.get('phone2',None)
        Office.email1 = request.POST.get('email1',None)
        Office.email2 = request.POST.get('email2',None)
        Office.save()
        return HttpResponseRedirect(reverse_lazy('offices'))