from django import template
import json

register = template.Library()

@register.filter
def split(value, key):
 """
   Returns the value turned into a list.
 """
 return value.split(key)

@register.filter
def numloop(limit):
 """
   Returns the value turned into a list.
 """
 return range(0,limit)

@register.filter
def loadjson(data):
    return json.loads(data)

@register.filter
def isDict(data):
    return isinstance(data,dict)

@register.filter
def isList(data):
    return isinstance(data,list)

@register.filter
def listByI(list,index):
    return list[int(index)]

@register.filter
def alterStr(str,rep):
    return str.replace(rep.split('=')[0],rep.split('=')[1])

@register.filter
def remBlank(str):
    return str.replace(' ','')