from django.db import models
from django.contrib.auth.models import User
 
# Create your models here.

# 
# # """ jki_menu """
# # class Menu(models.Model):
# #     menu_item_name = models.CharField(max_length=200, null=True, blank=True)
# #     menu_item_link = models.CharField(max_length=10, null=True, blank=True)
# #     menu_item_seq = models.IntegerField(max_length=2, default=0)
# #     
# #     class Meta:
# #       db_table = "jki_menu"
#       
#       
""" jki_sections """
class HomeBlocks(models.Model):
    name = models.CharField(max_length=10, null=True, blank=True)
    title = models.CharField(max_length=100, default=None)
    subtitle = models.CharField(max_length=250, default=None)
    featured_img = models.CharField(max_length=200, null=True, blank=True)
     
    class Meta:
      db_table = "jki_sections"


""" jki_philosophy """
class Philosophy(models.Model):
    block = models.ForeignKey(HomeBlocks, on_delete=models.CASCADE)
    title = models.CharField(max_length=200, null=True, blank=True)
    description = models.CharField(max_length=400, null=True, blank=True)
    icon = models.CharField(max_length=30, null=True, blank=True)
    feature = models.BooleanField(default=True)
     
    class Meta:
      db_table = "jki_philosophy"


""" jki_accolades """
class Accolades(models.Model):
    block = models.ForeignKey(HomeBlocks, on_delete=models.CASCADE)
    title = models.CharField(max_length=200, null=True, blank=True)
    description = models.CharField(max_length=200, null=True, blank=True)
    image = models.CharField(max_length=100, null=True, blank=True)
    feature = models.BooleanField(default=True)
    added = models.DateTimeField(auto_now_add=True)
     
    class Meta:
      db_table = "jki_accolades"


""" jki_students """
class Students(models.Model):  
    jkiuser = models.OneToOneField(User,on_delete=models.CASCADE)
    name = models.CharField(max_length=200, null=True, blank=True)
    email = models.CharField(max_length=100, null=True, blank=True)
    phone = models.CharField(max_length=20, null=True, blank=True)
    level = models.CharField(max_length=10, null=True, blank=True)
    image = models.CharField(max_length=100, null=True, blank=True)
    year = models.CharField(max_length=4, null=True, blank=True)
    added = models.DateTimeField(auto_now_add=True)
     
    class Meta:
      db_table = "jki_students"


""" jki_trainers """
class Trainers(models.Model):
    block = models.ForeignKey(HomeBlocks, on_delete=models.CASCADE)
    name = models.CharField(max_length=200, null=True, blank=True)
    bio = models.CharField(max_length=400, null=True, blank=True)
    email = models.CharField(max_length=100, null=True, blank=True)
    phone = models.CharField(max_length=20, null=True, blank=True)
    designation = models.CharField(max_length=200, null=True, blank=True)
    image = models.CharField(max_length=100, null=True, blank=True)
    facebook = models.CharField(max_length=100, null=True, blank=True)
    feature = models.BooleanField(default=True)
    added = models.DateTimeField(auto_now_add=True)
     
    class Meta:
      db_table = "jki_trainers"


""" jki_notices """
class Notices(models.Model):
    block = models.ForeignKey(HomeBlocks, on_delete=models.CASCADE)
    title = models.CharField(max_length=200, null=True, blank=True)
    description = models.CharField(max_length=200, null=True, blank=True)
    image = models.CharField(max_length=100, null=True, blank=True)
    url = models.CharField(max_length=250, null=True, blank=True)
    added = models.DateTimeField(auto_now_add=True)
    feature = models.BooleanField(default=True)
     
    class Meta:
      db_table = "jki_notices"
       
       
""" jki_footer """
class Footer(models.Model):
    about = models.CharField(max_length=300, null=True, blank=True)
    links = models.TextField(null=True, blank=True)
    address_text = models.CharField(max_length=100, null=True, blank=True)
    address_url = models.CharField(max_length=300, null=True, blank=True)
    phone1 = models.CharField(max_length=20, null=True, blank=True)
    phone2 = models.CharField(max_length=20, null=True, blank=True)
    email1 = models.CharField(max_length=100, null=True, blank=True)
    email2 = models.CharField(max_length=100, null=True, blank=True)
      
    class Meta:
      db_table = "jki_footer"
       
       
""" jki_offices """
class Offices(models.Model):
    title = models.CharField(max_length=100, null=True, blank=True)
    address = models.CharField(max_length=200, null=True, blank=True)
    gmap = models.CharField(max_length=300, null=True, blank=True)
    person = models.CharField(max_length=100, null=True, blank=True)
    phone1 = models.CharField(max_length=20, null=True, blank=True)
    phone2 = models.CharField(max_length=20, null=True, blank=True)
    email1 = models.CharField(max_length=100, null=True, blank=True)
    email2 = models.CharField(max_length=100, null=True, blank=True)
#     pointer = models.CharField(max_length=300, null=True, blank=True)
    added = models.DateTimeField(auto_now_add=True)
      
    class Meta:
      db_table = "jki_offices"
      
      
""" jki_gallery """
class Gallery(models.Model):  
    image = models.CharField(max_length=100, null=True, blank=True)
    uploaded = models.DateTimeField(auto_now_add=True)
    del_flag = models.BooleanField(default=False)
     
    class Meta:
      db_table = "jki_gallery"
      
      
""" jki_blogs """
class Blog(models.Model):  
    slug = models.CharField(max_length=20, null=True, blank=True)
    title = models.CharField(max_length=200, null=True, blank=True)
    body = models.CharField(max_length=200, null=True, blank=True)
    image = models.CharField(max_length=100, null=True, blank=True)
    url = models.CharField(max_length=250, null=True, blank=True)
    feature = models.BooleanField(default=False)
    added = models.DateTimeField(auto_now_add=True)
     
    class Meta:
      db_table = "jki_blogs"
      
      
""" jki_merchandise """
class Merchandise(models.Model):  
    title = models.CharField(max_length=100, null=True, blank=True)
    image = models.CharField(max_length=100, null=True, blank=True)
    added = models.DateTimeField(auto_now_add=True)
    url = models.CharField(max_length=250, null=True, blank=True)
    del_flag = models.BooleanField(default=False)
     
    class Meta:
      db_table = "jki_merchandise"