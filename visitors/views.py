import os
from django.conf import settings
from .models import *

from google.cloud import storage
from django.contrib.auth import authenticate, login as dj_login, logout

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.renderers import TemplateHTMLRenderer
from django.http.response import HttpResponseRedirect

# Google cloud storage api bucket declaration
gclient = storage.Client()
bucket = gclient.get_bucket(settings.GC_BUCKET)


'''
    @name: HomePage
    @param : request 
    @description : HomePage for all
'''   
class HomePage(APIView):
    renderer_classes = [TemplateHTMLRenderer]
    template_name = 'visitors/home.html'
     
    # rendering web template
    def get(self, request):
    #print([b.name for b in list(bucket.list_blobs(prefix='jki/images'))])
        return Response({ 
                'ifHome': 1,
                'title': 'Welcome to Japan Karate India',
                'gcburl': settings.GC_MEDIA_STORAGE,
                'banner' : HomeBlocks.objects.get(name='banner'), #banner section content
                'philosophy' : HomeBlocks.objects.get(name='philosophy'), #philosophy section content
                'codes' : Philosophy.objects.filter(feature=True), #list of featured codes
                'accolade' : HomeBlocks.objects.get(name='accolades'), #accolades section content
                'accolades' : Accolades.objects.filter(feature=True), #list of featured accolades
                'counter' : HomeBlocks.objects.get(name='counter'), #counters section content
                'counters' : [['Branches',13],['Trainers',11],['Students',500],['Black Belts',87]], #counters section content
                'trainer' : HomeBlocks.objects.get(name='trainers'), #trainers section content
                'trainers' : Trainers.objects.filter(feature=True), #list of featured trainers
                'notice' : HomeBlocks.objects.get(name='notices'), #announcements section content
                'notices' : Notices.objects.filter(feature=True), #list of featured announcements
                'footer' : Footer.objects.get(pk=1) #footer section content
              })


'''
    @name: LoginPage
    @param : request 
    @description : LoginPage for all
'''   
class LoginPage(APIView):
    renderer_classes = [TemplateHTMLRenderer]
    template_name = 'visitors/login.html'
     
    # rendering web template
    def get(self, request):
        print(settings.GC_MEDIA_STORAGE)
    #print([b.name for b in list(bucket.list_blobs(prefix='jki/images'))])
        return Response({ 
                'title': 'Login | Japan Karate India',
                'banner' : HomeBlocks.objects.get(name='banner'), #banner section content
                'gcburl': settings.GC_MEDIA_STORAGE,
              })
    
    def post(self, request):
        if request.user.is_anonymous:
            try:
                email = request.POST.get('user_email').lower()
                password = request.POST.get('user_pwd')
                next = request.POST.get('next')
                
                if User.objects.filter(email = email).exists():
                    username = User.objects.get(email = email).username
                    user = authenticate(username = username, password = password)
        
                    if user is not None:
                        dj_login(request, user)
                        request.session.set_expiry(3600)
                        return HttpResponseRedirect(reverse_lazy('banner')) 
                    else:
                        return Response("Login is invalid", status = 406) 
                else:
                    return Response("This email is not registered", status = 406)  
            except Exception as e:
                    return Response("Some error occurred", status = 400)  
        else:
            return Response("FORBIDDEN", status = 403)
        
'''
    @name: AboutPage
    @param : request 
    @description : AboutPage for all
'''   
class AboutPage(APIView):
    renderer_classes = [TemplateHTMLRenderer]
    template_name = 'visitors/about.html'
     
    # rendering web template
    def get(self, request):
    #print([b.name for b in list(bucket.list_blobs(prefix='jki/images'))])
        return Response({ 
                'ifAbout': 1,
                'title': 'About Us | Japan Karate India',
                'gcburl': settings.GC_MEDIA_STORAGE,
                'philosophy' : HomeBlocks.objects.get(name='philosophy'), #philosophy section content
                'codes' : Philosophy.objects.filter(feature=True), #list of featured codes
                'footer' : Footer.objects.get(pk=1) #footer section content
              })
        
'''
    @name: AccoladesPage
    @param : request 
    @description : AccoladesPage for all
'''   
class AccoladesPage(APIView):
    renderer_classes = [TemplateHTMLRenderer]
    template_name = 'visitors/accolades.html'
     
    # rendering web template
    def get(self, request):
    #print([b.name for b in list(bucket.list_blobs(prefix='jki/images'))])
        return Response({ 
                'ifAccolades': 1,
                'title': 'Accolades | Japan Karate India',
                'gcburl': settings.GC_MEDIA_STORAGE,
                'accolade' : HomeBlocks.objects.get(name='accolades'), #trainers section content
                'accolades' : Accolades.objects.filter(feature=True), #list of featured trainers
                'footer' : Footer.objects.get(pk=1) #footer section content
              })

'''
    @name: TrainersPage
    @param : request 
    @description : TrainersPage for all
'''   
class TrainersPage(APIView):
    renderer_classes = [TemplateHTMLRenderer]
    template_name = 'visitors/trainers.html'
     
    # rendering web template
    def get(self, request):
    #print([b.name for b in list(bucket.list_blobs(prefix='jki/images'))])
        return Response({ 
                'ifTrainers': 1,
                'title': 'Trainers | Japan Karate India',
                'gcburl': settings.GC_MEDIA_STORAGE,
                'trainer' : HomeBlocks.objects.get(name='trainers'), #trainers section content
                'trainers' : Trainers.objects.filter(feature=True), #list of featured trainers
                'footer' : Footer.objects.get(pk=1) #footer section content
              })
        

'''
    @name: NoticesPage
    @param : request 
    @description : NoticesPage for all
'''   
class NoticesPage(APIView):
    renderer_classes = [TemplateHTMLRenderer]
    template_name = 'visitors/notices.html'
     
    # rendering web template
    def get(self, request):
    #print([b.name for b in list(bucket.list_blobs(prefix='jki/images'))])
        return Response({ 
                'ifNotices': 1,
                'title': 'Announcements | Japan Karate India',
                'gcburl': settings.GC_MEDIA_STORAGE,
                'notice' : HomeBlocks.objects.get(name='notices'), #trainers section content
                'notices' : Notices.objects.filter(feature=True), #list of featured trainers
                'footer' : Footer.objects.get(pk=1) #footer section content
              })
        
'''
    @name: ContactPage
    @param : request 
    @description : ContactPage for all
'''   
class ContactPage(APIView):
    renderer_classes = [TemplateHTMLRenderer]
    template_name = 'visitors/contact.html'
     
    # rendering web template
    def get(self, request):
    #print([b.name for b in list(bucket.list_blobs(prefix='jki/images'))])
        return Response({ 
                'ifContact': 1,
                'title': 'Contact Us | Japan Karate India',
                'gcburl': settings.GC_MEDIA_STORAGE,
                'footer' : Footer.objects.get(pk=1) #footer section content
              })
        
#         
#         
# '''
#     @name: UserCreate
#     @param : request 
#     @description : LoginPage for all
# '''   
# class LoginPage(APIView):
#     renderer_classes = [TemplateHTMLRenderer]
#     template_name = 'visitors/login.html'
#      
#     # rendering web template
#     def get(self, request):
#         print(settings.GC_MEDIA_STORAGE)
#     #print([b.name for b in list(bucket.list_blobs(prefix='jki/images'))])
#         return Response({ 
#                 'title': 'Login | Japan Karate India',
#                 'banner' : HomeBlocks.objects.get(name='banner'), #banner section content
#                 'gcburl': settings.GC_MEDIA_STORAGE,
#               })
#     
#     def post(self, request):
#         if request.user.is_anonymous:
#             try:
#                 email = request.POST.get('user_email').lower()
#                 password = request.POST.get('user_pwd')
#                 next = request.POST.get('next')
#                 
#                 if User.objects.filter(email = email).exists():
#                     username = User.objects.get(email = email).username
#                     user = authenticate(username = username, password = password)
#         
#                     if user is not None:
#                         dj_login(request, user)
#                         request.session.set_expiry(3600)
#                         return HttpResponseRedirect(reverse_lazy('banner')) 
#                     else:
#                         return Response("Login is invalid", status = 406) 
#                 else:
#                     return Response("This email is not registered", status = 406)  
#             except Exception as e:
#                     return Response("Some error occurred", status = 400)  
#         else:
#             return Response("FORBIDDEN", status = 403)
        
# '''
#     @name: StudentsPage
#     @param : request 
#     @description : StudentsPage for all
# '''   
# class StudentsPage(APIView):
#     renderer_classes = [TemplateHTMLRenderer]
#     template_name = 'visitors/students.html'
#      
#     # rendering web template
#     def get(self, request):
#     #print([b.name for b in list(bucket.list_blobs(prefix='jki/images'))])
#         return Response({ 
#                 'ifStudents': 1,
#                 'title': 'Students | Japan Karate India',
#                 'gcburl': settings.GC_MEDIA_STORAGE,
#                 'student' : HomeBlocks.objects.get(name='students'), #trainers section content
#                 'students' : Students.objects.filter(feature=True), #list of featured trainers
#                 'footer' : Footer.objects.get(pk=1) #footer section content
#               })