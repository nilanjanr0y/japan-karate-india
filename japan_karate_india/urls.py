"""japan_karate_india URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from visitors.views import *
import cms.urls

urlpatterns = [
    path('admin/', admin.site.urls),

    path('', HomePage.as_view(), name="home"),
    path('login/', LoginPage.as_view(), name="login"),
    path('about-us/', AboutPage.as_view(), name="about"),
    path('accolades/', AccoladesPage.as_view(), name="accolades"),
    path('trainers/', TrainersPage.as_view(), name="trainers"),
#     path('students/', StudentsPage.as_view(), name="students"),
    path('announcements/', NoticesPage.as_view(), name="notices"),
    path('contact-us/', ContactPage.as_view(), name="contact"),
#     path('students/', include('students.urls')),
    path('cms/', include('cms.urls'))
]